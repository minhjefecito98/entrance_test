import React, { Suspense } from 'react';
import './app.scss';
// 
const Features = React.lazy(() => import("./features/index"));

function App() {
    return (
        <section className="app">
            <Suspense fallback={<div style={{ textAlign: "center" }}>Loading...</div>}>
                <Features />
            </Suspense>
        </section>
    );
}

export default App;
