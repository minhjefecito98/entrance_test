import React from 'react';
import moment from 'moment';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { Controller, useForm } from 'react-hook-form';



const defaultValues = {
    name: '',
    des: '',
    datepicker: moment().format('DD MMM YYYY'),
    status: 'normal',
    done: false
};
const Commonstyle = ({ type, onSubmit, detail }) => {
    const minDate = moment().format('DD MMM YYYY');

    const { control, handleSubmit, reset, formState: { errors } } = useForm({ defaultValues });
    const onSubmitFunction = (data) => {
        onSubmit(data)
        reset();
    }

    return (
        <React.Fragment>
            <form id="addTodos" onSubmit={handleSubmit(onSubmitFunction)}>
                {type === "add" ?
                    <React.Fragment>
                        <div className='common__content'>
                            <Controller
                                name="name"
                                rules={{ required: true }}
                                control={control}
                                render={({ field }) => <input {...field} className={errors?.name ? 'error' : ''} placeholder='Add new task ...' />}
                            />
                            <div className='gr'>
                                <label htmlFor="">Description</label>
                                <Controller
                                    name="des"
                                    control={control}
                                    render={({ field }) => <textarea cols="30" rows="10" {...field} placeholder='Lorem Ipsum....'></textarea>}
                                />
                            </div>
                            <div className='select-part'>
                                <div className='select-part__day common-style'>
                                    <label htmlFor="">Due Date</label>
                                    <Controller
                                        name="datepicker"
                                        control={control}
                                        render={({ field }) => <DatePickerComponent min={minDate} format="dd MMM yyyy" {...field} placeholder='Lorem Ipsum....' />}
                                    />
                                </div>
                                <div className='select-part__status common-style'>
                                    <label htmlFor="">Piority</label>
                                    <Controller
                                        name="status"
                                        control={control}
                                        render={({ field }) =>
                                            <select {...field}>
                                                <option value="low">low</option>
                                                <option value="normal">normal</option>
                                                <option value="high">high</option>
                                            </select>
                                        }
                                    />
                                </div>
                            </div>
                        </div>
                        <button type='submit' className='--add'>Add</button>
                    </React.Fragment>
                    :
                    <React.Fragment>
                        <div className='common__content'>
                            <Controller
                                name="name"
                                rules={{ required: true }}
                                control={control}
                                render={({ field }) => <input defaultValue={detail?.name} placeholder='Add new task ...' />}
                            />
                            <div className='gr'>
                                <label htmlFor="">Description</label>
                                <Controller
                                    name="des"
                                    control={control}
                                    render={({ field }) => <textarea defaultValue={detail?.des} cols="30" rows="10" placeholder='Lorem Ipsum....'></textarea>}
                                />
                            </div>
                            <div className='select-part'>
                                <div className='select-part__day common-style'>
                                    <label htmlFor="">Due Date</label>
                                    <Controller
                                        name="datepicker"
                                        control={control}
                                        render={({ field }) => <DatePickerComponent value={detail?.datepicker} min={minDate} format="dd MMM yyyy" placeholder='Lorem Ipsum....' />}
                                    />
                                </div>
                                <div className='select-part__status common-style'>
                                    <label htmlFor="">Piority</label>
                                    <Controller
                                        name="status"
                                        control={control}
                                        render={({ field }) =>
                                            <select defaultValue={detail?.status}>
                                                <option value="low">low</option>
                                                <option value="normal">normal</option>
                                                <option value="high">high</option>
                                            </select>
                                        }
                                    />
                                </div>
                            </div>
                        </div>
                        <button type='submit' className='--update'>Update</button>
                    </React.Fragment>
                }
            </form>
        </React.Fragment>
    );
}

export default Commonstyle;
