import moment from 'moment';
import React, { useState } from 'react';
import ListTodo from '../ListTodo/index';
import './index.scss';
import Commonstyle from '../../components/common/CommonStyle';

const AddTodo = () => {
    const [todosList, setTodosList] = useState(() => {
        const savedTodos = localStorage.getItem("todos");
        if (savedTodos) {
            return JSON.parse(savedTodos);
        } else {
            return [];
        }
    });
    const onSubmit = (data, e) => {
        setTodosList([
            ...todosList,
            {
                id: moment().unix(),
                name: data.name.trim(),
                des: data.des.trim(),
                datepicker: moment(data.datepicker).format('DD MMM YYYY'),
                status: data.status,
                done: false
            }
        ]);
    }
    const handleGetChangeTodos = (todosChanged) => {
        setTodosList(todosChanged);
    }

    return (
        <React.Fragment>
            <div className='common-todo__container add-todo__container'>
                <h2>New Task</h2>
                <Commonstyle onSubmit={onSubmit} type="add" />
            </div>
            <ListTodo todosList={todosList} handleGetChangeTodos={handleGetChangeTodos} />
        </React.Fragment>

    );
};


export default AddTodo;
