import React, { useEffect, useState } from 'react';
import moment from 'moment';
import './index.scss';
import PropTypes from 'prop-types';
import Commonstyle from '../../components/common/CommonStyle';


const ListTodo = ({ todosList, handleGetChangeTodos }) => {
    const [isCheck, setIsCheck] = useState([]);
    const [showBulk, setShowBulk] = useState(false);
    const [showDetail, setShowDetail] = useState({
        show: false,
        id: null
    });
    const [list, setList] = useState([]);

    const handleShowDetail = (e, id) => {
        e.preventDefault();
        showDetail.id === id ? setShowDetail({ id: null, show: false }) : setShowDetail({ id: id, show: true });
    }
    const handleDoneTodoSelected = (e, id) => {
        // let newTodos = todosList.map(task => task.id === id ? { ...task, done: !task.done } : { ...task });
        let newTodos = todosList.map(task => isCheck.includes(task.id.toString()) ? { ...task, done: !task.done } : { ...task });
        setList(newTodos);
        setIsCheck([]);
        handleGetChangeTodos(newTodos);
        setShowDetail({ id: null, show: false })
    }
    const handleDeleteTodoSelected = (e, id, done) => {
        e.preventDefault();
        let newTodos = [...todosList];
        if (done !== true) {
            newTodos = id ? todosList.filter(f => f.id !== id) : todosList.filter(f => !isCheck.includes(f.id.toString()));
        } else {
            alert('Task đã hoàn thành, không thể xoá!');
            return newTodos;
        }
        const countDone = newTodos.filter(f => f.done === true);
        if (!id && countDone && countDone < todosList) setShowBulk(true);
        setList(newTodos);
        handleGetChangeTodos(newTodos);
        setShowDetail({ id: null, show: false })
    }
    const handleSearch = (e) => {
        const newTodos = e.target.value.trim() ? todosList.filter(f => f.name.toLowerCase().includes(e.target.value.trim().toLowerCase())) : todosList;
        setList(newTodos);
    }
    const checkIsChecked = (e) => {
        const { id, checked } = e.target;
        setIsCheck([...isCheck, id]);
        if (!checked) {
            setIsCheck(isCheck.filter(item => item !== id));
        }
    }
    // 
    const onSubmit = (data, e) => {
        console.log(e);
        // let newTodos = [];
        // console.log(newTodos);
        // setList(newTodos);
        // setShowDetail({ id: null, show: false });
        // handleGetChangeTodos(newTodos);
    }
    useEffect(() => {
        setList(todosList);
        localStorage.setItem('todos', JSON.stringify(todosList));
    }, [todosList]);
    useEffect(() => {
        localStorage.setItem('todos', JSON.stringify(list));
    }, [list]);
    useEffect(() => {
        isCheck.length > 0 ? setShowBulk(true) : setShowBulk(false);
    }, [isCheck]);


    return (
        <section className='common-todo__container list-todo__container'>
            <h2>Todo List</h2>
            <f-content>
                <div className='form__content'>
                    <input type="text" placeholder='Search ...' onChange={(e) => handleSearch(e)} />
                    {list?.sort((a, b) => moment(a.datepicker).format('YYYYMMDD') - moment(b.datepicker).format('YYYYMMDD')).map(value => {
                        return <div className='each-task' key={'list_' + value.id} style={{ pointerEvents: value.done ? 'none' : '', textDecoration: value.done ? 'line-through' : '', background: value.done ? '#8080808a' : '' }}>
                            <div className='each-task__content'>
                                <div className='input'>
                                    {value.done ? <input type="checkbox" defaultChecked={true} style={{ zIndex: '-1' }} /> : <input type="checkbox" id={value.id} onChange={(e) => checkIsChecked(e)} />}
                                    <span>{value?.name}</span>
                                </div>
                                <div className='function'>
                                    <button type='button' className='--detail' onClick={(e) => handleShowDetail(e, value.id)}>Detail</button>
                                    <button type='button' className='--remove' onClick={(e) => handleDeleteTodoSelected(e, value.id, value.done)}>Remove</button>
                                </div>
                            </div>
                            <div className='each-task__detail' style={{ display: showDetail.show && showDetail.id === value.id ? '' : 'none' }}>
                                <Commonstyle onSubmit={onSubmit} detail={value} />
                            </div>
                        </div>
                    })}
                </div>
                <div className='bulk-action' style={{ display: showBulk ? '' : 'none' }}>
                    <span>Bulk Action:</span>
                    <b-function>
                        <button type="button" className='--done' onClick={handleDoneTodoSelected}>Done</button>
                        <button type="button" className='--remove' onClick={handleDeleteTodoSelected}>Remove</button>
                    </b-function>
                </div>
            </f-content>
        </section>
    );
};


ListTodo.propTypes = {
    handleGetChangeTodos: PropTypes.func.isRequired,
    todosList: PropTypes.array
}

export default ListTodo;
